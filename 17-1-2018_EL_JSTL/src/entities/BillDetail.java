package entities;

public class BillDetail {
	private int billCode;
	private String productCode;
	private int nums;
	
	public BillDetail(int billCode, String productCode, int nums) {
		this.billCode = billCode;
		this.productCode = productCode;
		this.nums = nums;
	}
	
	public BillDetail() {
		this.billCode = 0;
		this.productCode = "";
		this.nums = 0;
	}

	public int getBillCode() {
		return billCode;
	}

	public void setBillCode(int billCode) {
		this.billCode = billCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public int getNums() {
		return nums;
	}

	public void setNums(int nums) {
		this.nums = nums;
	}
}
