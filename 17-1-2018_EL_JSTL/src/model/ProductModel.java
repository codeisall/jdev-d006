package model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.Part;

import com.mysql.jdbc.Connection;

import MyConnect.MyConnect;
import entities.Product;

public class ProductModel {
	public Product pro;
	Part file;
	private int testgit;
	
	public ProductModel(){}
	
	public ProductModel(Product pro){
		this.pro = pro;
	}
	
	public ProductModel(Part file){
		this.file = file;
	}
	
	public ArrayList<Product> getListProduct(){
		ArrayList<Product> listPro = new ArrayList<>();
		Connection cn = new MyConnect().getcn();
		if (cn == null) return null;
		
		String sql = "select * from product";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			ResultSet rs = pt.executeQuery();
			while (rs.next()){
				this.pro = new Product(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getString(5));
				listPro.add(pro);
			}
			pt.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listPro;
	}
	
	public Product getProcuctByCode(String productCode){
		Product proGot = null;
		Connection cn = new MyConnect().getcn();
		if (cn == null) return null;
		
		String sql = "select * from product "
				+ "where productcode like ?";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, productCode);
			ResultSet rs = pt.executeQuery();
			if (rs.next())
				proGot = new Product(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getString(5));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return proGot;
	}
	
	public int update() {
		int result = 0;
		Connection cn = new MyConnect().getcn();
		String sql = "update product "
				+ "set productname = ?, cost = ? "
				+ "where productcode like ? and catecode like ?";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setString(1, pro.getProductName());
			pt.setInt(2, pro.getProductCost());
			pt.setString(3, pro.getProductCode());
			pt.setString(4, pro.getCateCode());
			result = pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	//Upload Image
	public String getFileName(Part filepart)
	{
	     String filename="";
	     String header = filepart.getHeader("Content-Disposition");
	     //System.out.println("header:" + header);
	     int beginIndex = header.lastIndexOf("=");
	     filename = header.substring(beginIndex+1);
	                    
	     //remove "" quotes 2 dau chuoi
	     Pattern p = Pattern.compile("\"([^\"]*)\"");
	     Matcher m = p.matcher(filename);
	     while (m.find()) 
	            filename = m.group(1);
	                    
	     //danh cho IE 
	     beginIndex = filename.lastIndexOf("\\");
	     filename = filename.substring(beginIndex+1);
	     //System.out.println("filename:" + filename);
	 
	     return filename;
	}
	 

	public void uploadFile(String uploadRootPath)
	{                          
		try
		{
		     InputStream fis = file.getInputStream();
		     byte[]data = new byte[fis.available()];
		     fis.read(data);
		                        
		     FileOutputStream out = new FileOutputStream(new File( uploadRootPath + "\\" + getFileName(file)));
		     out.write(data);
		                        
		     out.close();
		}
		catch(IOException e)
		{
		     e.printStackTrace();
		     System.out.println("That bai");
		}
		System.out.println("Thanh cong");
	}
}
