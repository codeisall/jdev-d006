package model;

import java.util.ArrayList;
import java.util.HashMap;

import entities.Items;

public class CartModel {
	private HashMap<String, Items> cart;

	public CartModel(HashMap<String, Items> cart) {
		this.cart = cart;
	}
	
	public CartModel() {
		this.cart = new HashMap<>();
	}

	public HashMap<String, Items> getCart() {
		return cart;
	}

	public void setCart(HashMap<String, Items> cart) {
		this.cart = cart;
	}
	
	public void addItem(String productCode){
		if (this.cart.containsKey(productCode)){
			this.cart.get(productCode).setNums(cart.get(productCode).getNums()+1);
			//tang so luon len
		}else{
			this.cart.put(productCode, new Items(1, new ProductModel().getProcuctByCode(productCode)));
		}
	}
	
	public void removeProduct(String productCode){
		if (this.cart.get(productCode).getNums() >= 2){
			this.cart.get(productCode).setNums(this.cart.get(productCode).getNums()-1);
		}else{
			this.cart.remove(productCode);
		}
	}
	
	public int totalListProct(){
		int total = 0;
		for (Items item : cart.values()){
			total = total + item.getPro().getProductCost()*item.getNums();
		}
		return total;
	}
	
	public ArrayList<Items> getListItems(){
		ArrayList<Items> listItem = new ArrayList<>();
		for (Items item : cart.values()){
			listItem.add(item);
		}
		return listItem;
	}
	
	public int getNumPorduct() {
		int nums = 0;
		for (Items i : cart.values()) {
			nums += i.getNums();
		}
		return nums;
	}
}
