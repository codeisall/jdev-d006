package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import MyConnect.MyConnect;
import entities.BillDetail;

public class BillDetailModel {
	private BillDetail billDetail;

	public BillDetailModel(BillDetail billDetail) {
		this.billDetail = billDetail;
	}
	
	public BillDetailModel() {
	}
	
	public int insert() {
		int result = 0;
		Connection cn = new MyConnect().getcn();
		String sql = "insert into billdetail "
				+ "values(?, ? ,?)";
		try {
			PreparedStatement pt = cn.prepareStatement(sql);
			pt.setInt(1, billDetail.getBillCode());
			pt.setString(2, billDetail.getProductCode());
			pt.setInt(3, billDetail.getNums());
			result = pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}
