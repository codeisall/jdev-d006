package cotroller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import MyConnect.MyConnect;
import entities.Bill;
import entities.BillDetail;
import entities.Items;
import model.BillDetailModel;
import model.BillModel;
import model.CartModel;

/**
 * Servlet implementation class manageShopping
 */
@WebServlet("/manageShopping")
public class manageShopping extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private Connection cn;
    private CartModel cart;
    private BillDetailModel billDetailModel;
    private BillModel billModel;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public manageShopping() {
        super();
        cn = new MyConnect().getcn();
        cart = new CartModel();
        billDetailModel = new BillDetailModel();
        billModel = new BillModel();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Phải để cartmodel là global variable để nó vẫn là 1 cart
		//nếu để trong doPost thì mỗi lần có action nó sẽ tạo mới cart
		String task = request.getParameter("task");
		String productCode = "";
		String page = "";
		if (task.equals("add")) {
			if (request.getParameter("btnAdd") != null) {
				//khi ấn btnShowCart thif btnAdd null
				//nếu null add vào thì ko có sản phẩn và in ra là ko có gì
				//phải tránh nó khi ấn btnShowCart
				//Nếu btnAdd khác null thì mới add
				productCode = request.getParameter("btnAdd");
				System.out.println(productCode);
				cart.addItem(productCode);
			}
			if (cart.getNumPorduct() > 0) {
				//tránh trường hợp ấn btnAdd thì nó có 1 sp và lúc đó
				//chưa ấn btnShowCart nên btnShowCart null
				//khi nào ấn mới có giá trị
				
				//khi số sản phẩm lớn 0 thì mới xét đến btn xem cart
				if (request.getParameter("btnShowCart") == null)
					page = "view.jsp";
				else {
					page = request.getParameter("btnShowCart");
					request.setAttribute("listCart", cart.getListItems());
				}
			}else {
				page = "view.jsp";
			}
			if (request.getParameter("btnUpdate") != null) {
				page = "updateproduct.jsp";
				Connection cn = new MyConnect().getcn();
				productCode = request.getParameter("btnUpdate");
				String sql = "select * from product, category "
						+ "where category.cateCode like product.cateCode "
						+ "and product.productCode like ?";
				try {
					PreparedStatement pt = cn.prepareStatement(sql);
					pt.setString(1, productCode);
					ResultSet rs = pt.executeQuery();
					if (rs.next()) {
						request.setAttribute("productName", rs.getString(2));
						request.setAttribute("productCost", rs.getInt(3));
						request.setAttribute("productCode", rs.getString(1));
						request.setAttribute("cateCode", rs.getString(5));
						request.setAttribute("image", rs.getString(4));
					}
					pt.close();
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else if (task.equals("remove")) {
			if (request.getParameter("btnDelete") != null) {
				cart.removeProduct(request.getParameter("btnDelete"));
				request.setAttribute("listCart", cart.getListItems());
				page = "viewcart.jsp";
			}
			if (request.getParameter("btnShopping") != null) {
				page = request.getParameter("btnShopping");
			}
			if (request.getParameter("btnRemoveCart") != null) {
				cart.getListItems().clear();
				cart.getCart().clear();
				page = request.getParameter("btnRemoveCart");
			}
			if (request.getParameter("btnBuy") != null) {
				Random rd = new Random();
				ArrayList<Integer> listBillCode = new ArrayList<>();
				String sqlGetBillCode = "select billcode from bill";
				try {
					PreparedStatement pt = cn.prepareStatement(sqlGetBillCode);
					ResultSet rs = pt.executeQuery();
					while (rs.next()) {
						listBillCode.add(rs.getInt(1));
					}
					pt.close();
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat date = new SimpleDateFormat("YYY-MM-DD");
				billModel = new BillModel(new Bill(0, date.format(cal.getTime())));
				if (billModel.insert() != 0) {
					int newestBillCode = new BillModel().getNewestBill();
					BillDetail billDetail;
					for (Items i : cart.getListItems()) {
						billDetail = new BillDetail(newestBillCode, i.getPro().getProductCode(), i.getNums());
						billDetailModel = new BillDetailModel(billDetail);
						billDetailModel.insert();
					}
					cart.getListItems().clear();
					cart.getCart().clear();
					page = request.getParameter("btnBuy");
				}
			}
		}
		request.setAttribute("numProduct", cart.getNumPorduct());
		request.getRequestDispatcher(page).forward(request, response);
	}

}
