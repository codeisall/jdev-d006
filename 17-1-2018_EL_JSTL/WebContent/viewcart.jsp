<%@page import="entities.Items"%>
<%@page import="entities.Product"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cart</title>
</head>
<body>
	<form action="manageShopping?task=remove" method="post">
		<h1 align="center">Your Cart</h1>
		
		<table border="2px" align="center">
			<tr>
				<th>Product Code</th>
				<th>Product Name</th>
				<th>Product Cost</th>
				<th>Quantity</th>
				<th>Total</th>
				<th>Image</th>
				<th>Action</th>
			</tr>
			<%-- <%ArrayList<Items> cart = (ArrayList<Items>) request.getAttribute("listCart");%> --%>
			<c:set var="total" value="0"></c:set>
			<c:forEach var="item" items="${requestScope.listCart}">
				<tr>
					<td align="center">${item.pro.productCode}</td>
					<td align="center">${item.pro.productName}</td>
					<td align="center">${item.pro.productCost}</td>
					<td align="center">${item.nums}</td>
					<td align="center">${item.pro.productCost*item.nums}</td>
					<c:set var="cost" value="${item.pro.productCost*item.nums}"></c:set>
					<c:set var="total" value="${total + cost}"></c:set>
					<td align="center"><img alt="Phone" src="Images/${item.pro.image}" height="150px" width="120px"></td>
					<td align="center"><button type="submit" name="btnDelete" value="${item.pro.productCode}">Delete</button></td>
				</tr>
			</c:forEach>
		</table><br>
		
		<table align="center">
			<tr>
				<th></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<td colspan="3"><p align="center"><strong>Total : <c:out value="${total}"></c:out></strong></p></td>
			</tr>
			<tr>
				<%-- <c:url var="shopping" value="view.jsp"></c:url> --%>
				<%-- <td><a href="${shopping}">Shopping</a></td> --%>
				<td align="center"><button type="submit" name="btnShopping" value="view.jsp">Shopping</button></td>
				<p>|</p>
				<td align="center"><button type="submit" name="btnRemoveCart" value="viewcart.jsp">Remove cart</button></td>
				<p>|</p>
				<td align="center"><button type="submit" name="btnBuy" value="viewcart.jsp">Buy</button></td>
			</tr>
		</table>
	</form>
</body>
</html>