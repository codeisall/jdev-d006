<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="ProductServlet?action=update" method="post">	
		<h1 align="center">Update Product</h1>
		<table align="center">
			<tr>
				<th align="center">1</th>
				<td align="center"><input readonly="readonly" type="text" name="txtCategoryCode" value="${requestScope.cateCode}">
			</tr>
			<tr>
				<th align="center">Product Code</th>
				<td align="center"><input readonly="readonly" type="text" name="txtProductCode" value="${requestScope.productCode}">
			</tr>
			<tr>
				<th align="center">Product Name</th>
				<td align="center"><input type="text" name="txtProductName" value="${requestScope.productName}">
			</tr>
			<tr>
				<th align="center">Product Cost</th>
				<td align="center"><input type="text" name="txtProductCost" value="${requestScope.productCost}">
			</tr>
			<tr>
				<td align="center" colspan="2"><img alt="Phone" src="Images/${requestScope.image}" name="currentImage" height="150px" width="120px"></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><input type="file" name="fileImage"></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><input type="submit" value="Submit"></td>
			</tr>
		</table>
	</form>
</body>
</html>