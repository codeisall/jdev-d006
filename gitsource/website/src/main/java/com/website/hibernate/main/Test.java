package com.website.hibernate.main;

import java.util.ArrayList;

import org.hibernate.classic.Session;

import com.website.hibernate.entities.Address;
import com.website.hibernate.entities.Course;
import com.website.hibernate.entities.Student;
import com.website.hibernate.persistence.HibernateUtil;

public class Test {

	public static void main(String[] args) {
		System.out.println("Maven + Hibernate + MySQL");
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		session.getTransaction().begin();
		Student student = new Student();
		Address address = new Address();
		Course courses = new Course();
		Course courses2 = new Course();
		ArrayList<Course> list = new ArrayList<Course>();
		courses.setId(1);
		courses.setCourseName("Java");
		list.add(courses);
		courses2.setId(2);
		courses2.setCourseName("PDM");
		list.add(courses2);
		student.setFirstName("Nguyen Lam");
		student.setLastName("Michael");
		student.setAge(19);
		
		address.setStreet("Linh Trung");
		address.setDistrict("Thu Duc");
		address.setCity("Ho Chi Minh");
		student.setAddress(address);
		student.setCourses(list);
		session.save(student);
		
		session.getTransaction().commit();
	}

}
